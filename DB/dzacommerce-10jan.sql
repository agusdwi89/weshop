/*
 Navicat Premium Data Transfer

 Source Server         : dzahost
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : dzacommerce

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 10/01/2019 01:55:39
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for coupon
-- ----------------------------
DROP TABLE IF EXISTS `coupon`;
CREATE TABLE `coupon` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL DEFAULT '',
  `valid` date NOT NULL,
  `discount` float DEFAULT NULL,
  `active` enum('yes','no') NOT NULL DEFAULT 'yes',
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for list_section
-- ----------------------------
DROP TABLE IF EXISTS `list_section`;
CREATE TABLE `list_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `section_id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `type_sec_id` int(11) NOT NULL,
  `sec_order` int(11) NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mst_city
-- ----------------------------
DROP TABLE IF EXISTS `mst_city`;
CREATE TABLE `mst_city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_province` int(11) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9472 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for mst_delivery
-- ----------------------------
DROP TABLE IF EXISTS `mst_delivery`;
CREATE TABLE `mst_delivery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_city` int(11) DEFAULT NULL,
  `jne` int(11) NOT NULL DEFAULT '0',
  `jnt` int(11) NOT NULL DEFAULT '0',
  `ninja` int(11) NOT NULL DEFAULT '0',
  `wahana` int(11) NOT NULL,
  `pos` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=513 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for mst_payment
-- ----------------------------
DROP TABLE IF EXISTS `mst_payment`;
CREATE TABLE `mst_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('disable','enable','','') COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mst_payment_transfer
-- ----------------------------
DROP TABLE IF EXISTS `mst_payment_transfer`;
CREATE TABLE `mst_payment_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bank` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_holder` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for mst_province
-- ----------------------------
DROP TABLE IF EXISTS `mst_province`;
CREATE TABLE `mst_province` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=95 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for mst_transfer
-- ----------------------------
DROP TABLE IF EXISTS `mst_transfer`;
CREATE TABLE `mst_transfer` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `bank` varchar(200) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `number` varchar(200) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cust_f_name` varchar(255) DEFAULT NULL,
  `cust_l_name` varchar(255) DEFAULT NULL,
  `cust_email` varchar(255) DEFAULT NULL,
  `cust_address` text,
  `cust_province` varchar(255) DEFAULT NULL,
  `cust_city` varchar(255) DEFAULT NULL,
  `cust_zip` varchar(255) DEFAULT NULL,
  `cust_phone` varchar(255) DEFAULT NULL,
  `cust_shipping` varchar(255) DEFAULT NULL,
  `payment` varchar(255) DEFAULT NULL,
  `payment_detail` varchar(255) DEFAULT NULL,
  `prd_id` int(11) DEFAULT NULL,
  `prd_title` varchar(255) DEFAULT NULL,
  `prd_description` varchar(255) DEFAULT NULL,
  `prd_description2` varchar(255) DEFAULT NULL,
  `prd_image` varchar(255) DEFAULT NULL,
  `prd_price_tag` varchar(255) DEFAULT NULL,
  `prd_price` float(10,2) DEFAULT NULL,
  `prd_shipment` float(10,2) DEFAULT NULL,
  `prd_coupon` float(10,2) DEFAULT NULL,
  `prd_coupon_code` varchar(255) DEFAULT NULL,
  `prd_total` float(255,0) DEFAULT NULL,
  `link_unique` varchar(50) NOT NULL,
  `order_status` enum('wait-payment','payment-success','delivery-process','completed') DEFAULT 'wait-payment',
  `time_order` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `resi` varchar(255) DEFAULT NULL,
  `receipt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_about
-- ----------------------------
DROP TABLE IF EXISTS `section_about`;
CREATE TABLE `section_about` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `jargon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image_position` enum('left','right') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'right',
  `button_text` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `button_link` varchar(200) COLLATE utf8_unicode_ci NOT NULL DEFAULT '#',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_buy
-- ----------------------------
DROP TABLE IF EXISTS `section_buy`;
CREATE TABLE `section_buy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `button_text` varchar(255) DEFAULT NULL,
  `button_link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_download
-- ----------------------------
DROP TABLE IF EXISTS `section_download`;
CREATE TABLE `section_download` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title1` varchar(255) DEFAULT NULL,
  `title2` varchar(255) DEFAULT NULL,
  `title3` varchar(255) DEFAULT NULL,
  `description` text,
  `image_bg` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link_apple` varchar(255) DEFAULT NULL,
  `image_apple` varchar(255) DEFAULT NULL,
  `link_android` varchar(255) DEFAULT NULL,
  `image_android` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_faq
-- ----------------------------
DROP TABLE IF EXISTS `section_faq`;
CREATE TABLE `section_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_faq_items
-- ----------------------------
DROP TABLE IF EXISTS `section_faq_items`;
CREATE TABLE `section_faq_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sfaq_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_features
-- ----------------------------
DROP TABLE IF EXISTS `section_features`;
CREATE TABLE `section_features` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `image_position` enum('left','right','mid') COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_features_items
-- ----------------------------
DROP TABLE IF EXISTS `section_features_items`;
CREATE TABLE `section_features_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sf_id` int(11) NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_footer
-- ----------------------------
DROP TABLE IF EXISTS `section_footer`;
CREATE TABLE `section_footer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `l_title` varchar(255) DEFAULT NULL,
  `l_description` varchar(255) DEFAULT NULL,
  `l_bottom_text` varchar(255) DEFAULT NULL,
  `r_title` varchar(255) DEFAULT NULL,
  `r_description` varchar(255) DEFAULT NULL,
  `r_phone` varchar(255) DEFAULT NULL,
  `r_email` varchar(255) DEFAULT NULL,
  `r_address` varchar(255) DEFAULT NULL,
  `r_image` varchar(255) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_footer_links
-- ----------------------------
DROP TABLE IF EXISTS `section_footer_links`;
CREATE TABLE `section_footer_links` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sfoot_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_footer_social
-- ----------------------------
DROP TABLE IF EXISTS `section_footer_social`;
CREATE TABLE `section_footer_social` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sfoot_id` int(11) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  `facebook` varchar(100) NOT NULL DEFAULT '',
  `twitter` varchar(100) NOT NULL DEFAULT '',
  `google` varchar(100) NOT NULL DEFAULT '',
  `instagram` varchar(100) NOT NULL DEFAULT '',
  `behance` varchar(100) NOT NULL DEFAULT '',
  `dribbble` varchar(100) NOT NULL DEFAULT '',
  `linkedin` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_gallery
-- ----------------------------
DROP TABLE IF EXISTS `section_gallery`;
CREATE TABLE `section_gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `list` text NOT NULL,
  `jargon` varchar(255) NOT NULL DEFAULT '',
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_gallery_items
-- ----------------------------
DROP TABLE IF EXISTS `section_gallery_items`;
CREATE TABLE `section_gallery_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `sgal_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '',
  `title_secondary` varchar(255) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_header
-- ----------------------------
DROP TABLE IF EXISTS `section_header`;
CREATE TABLE `section_header` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_first` mediumtext COLLATE utf8_unicode_ci NOT NULL,
  `title_highlight` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title_last` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `button_link` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `button_text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color_main` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color_secondary` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `color_button` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_newsletter
-- ----------------------------
DROP TABLE IF EXISTS `section_newsletter`;
CREATE TABLE `section_newsletter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `spam_text` varchar(255) DEFAULT NULL,
  `placeholder_text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_product
-- ----------------------------
DROP TABLE IF EXISTS `section_product`;
CREATE TABLE `section_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `type` enum('type_1','type_2','type_3') NOT NULL DEFAULT 'type_1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_product_items
-- ----------------------------
DROP TABLE IF EXISTS `section_product_items`;
CREATE TABLE `section_product_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prd_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL,
  `description2` varchar(200) DEFAULT NULL,
  `price` float DEFAULT '0',
  `price_dsc` float DEFAULT '0',
  `image` varchar(200) NOT NULL,
  `image_bg` varchar(200) NOT NULL,
  `available` enum('yes','no') DEFAULT 'yes',
  `active` enum('yes','no') DEFAULT 'yes',
  `code` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_review
-- ----------------------------
DROP TABLE IF EXISTS `section_review`;
CREATE TABLE `section_review` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `title_secondary` varchar(255) DEFAULT 'Our Customers',
  `description` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_review_customers
-- ----------------------------
DROP TABLE IF EXISTS `section_review_customers`;
CREATE TABLE `section_review_customers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srev_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `link` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_review_items
-- ----------------------------
DROP TABLE IF EXISTS `section_review_items`;
CREATE TABLE `section_review_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `srev_id` int(11) DEFAULT NULL,
  `review` text,
  `name` varchar(255) DEFAULT NULL,
  `position` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_team
-- ----------------------------
DROP TABLE IF EXISTS `section_team`;
CREATE TABLE `section_team` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `description` text,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_team_items
-- ----------------------------
DROP TABLE IF EXISTS `section_team_items`;
CREATE TABLE `section_team_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `steam_id` int(11) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `photo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for section_technologies
-- ----------------------------
DROP TABLE IF EXISTS `section_technologies`;
CREATE TABLE `section_technologies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `date_created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_technologies_items
-- ----------------------------
DROP TABLE IF EXISTS `section_technologies_items`;
CREATE TABLE `section_technologies_items` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `st_id` int(11) NOT NULL,
  `icon` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for section_video
-- ----------------------------
DROP TABLE IF EXISTS `section_video`;
CREATE TABLE `section_video` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link` varchar(255) DEFAULT NULL,
  `date_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for site_config
-- ----------------------------
DROP TABLE IF EXISTS `site_config`;
CREATE TABLE `site_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `value` text,
  `value2` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Table structure for top_menu
-- ----------------------------
DROP TABLE IF EXISTS `top_menu`;
CREATE TABLE `top_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link` enum('internal','eksternal') COLLATE utf8_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `target` enum('inside','blank') COLLATE utf8_unicode_ci DEFAULT NULL,
  `button` enum('yes','no') COLLATE utf8_unicode_ci DEFAULT 'no',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Table structure for type_section
-- ----------------------------
DROP TABLE IF EXISTS `type_section`;
CREATE TABLE `type_section` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `db_table` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- View structure for get_list_section_order
-- ----------------------------
DROP VIEW IF EXISTS `get_list_section_order`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `get_list_section_order` AS select `list_section`.`sec_order` AS `sec_order` from `list_section` order by `list_section`.`sec_order` desc limit 1;

-- ----------------------------
-- View structure for view_list_section
-- ----------------------------
DROP VIEW IF EXISTS `view_list_section`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_list_section` AS select `ls`.`id` AS `id`,`ls`.`section_id` AS `section_id`,`ls`.`name` AS `name`,`ls`.`type_sec_id` AS `type_sec_id`,`ls`.`sec_order` AS `sec_order`,`ls`.`date_created` AS `date_created`,`ts`.`name` AS `ts_name`,`ts`.`db_table` AS `db_table` from (`list_section` `ls` join `type_section` `ts`) where (`ls`.`type_sec_id` = `ts`.`id`) order by `ls`.`sec_order`;

-- ----------------------------
-- View structure for view_orders
-- ----------------------------
DROP VIEW IF EXISTS `view_orders`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `view_orders` AS select `orders`.`id` AS `id`,`orders`.`cust_f_name` AS `cust_f_name`,`orders`.`cust_l_name` AS `cust_l_name`,`orders`.`cust_email` AS `cust_email`,`orders`.`cust_address` AS `cust_address`,`orders`.`cust_province` AS `cust_province`,`orders`.`cust_city` AS `cust_city`,`orders`.`cust_zip` AS `cust_zip`,`orders`.`cust_phone` AS `cust_phone`,`orders`.`cust_shipping` AS `cust_shipping`,`orders`.`payment` AS `payment`,`orders`.`payment_detail` AS `payment_detail`,`orders`.`prd_id` AS `prd_id`,`orders`.`prd_title` AS `prd_title`,`orders`.`prd_description` AS `prd_description`,`orders`.`prd_description2` AS `prd_description2`,`orders`.`prd_image` AS `prd_image`,`orders`.`prd_price_tag` AS `prd_price_tag`,`orders`.`prd_price` AS `prd_price`,`orders`.`prd_shipment` AS `prd_shipment`,`orders`.`prd_coupon` AS `prd_coupon`,`orders`.`prd_coupon_code` AS `prd_coupon_code`,`orders`.`prd_total` AS `prd_total`,`orders`.`link_unique` AS `link_unique`,`orders`.`time_order` AS `time_order`,`mst_province`.`province` AS `province`,`mst_city`.`city` AS `city` from ((`orders` join `mst_province` on((`orders`.`cust_province` = `mst_province`.`id`))) join `mst_city` on((`orders`.`cust_city` = `mst_city`.`id`)));

-- ----------------------------
-- View structure for v_manage_order
-- ----------------------------
DROP VIEW IF EXISTS `v_manage_order`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_manage_order` AS select `orders`.`id` AS `id`,`orders`.`cust_f_name` AS `cust_f_name`,`orders`.`cust_l_name` AS `cust_l_name`,`orders`.`cust_email` AS `cust_email`,`orders`.`cust_address` AS `cust_address`,`orders`.`cust_province` AS `cust_province`,`orders`.`cust_city` AS `cust_city`,`orders`.`cust_zip` AS `cust_zip`,`orders`.`cust_phone` AS `cust_phone`,`orders`.`cust_shipping` AS `cust_shipping`,`orders`.`payment` AS `payment`,`orders`.`payment_detail` AS `payment_detail`,`orders`.`prd_id` AS `prd_id`,`orders`.`prd_title` AS `prd_title`,`orders`.`prd_description` AS `prd_description`,`orders`.`prd_description2` AS `prd_description2`,`orders`.`prd_image` AS `prd_image`,`orders`.`prd_price_tag` AS `prd_price_tag`,`orders`.`prd_price` AS `prd_price`,`orders`.`prd_shipment` AS `prd_shipment`,`orders`.`prd_coupon` AS `prd_coupon`,`orders`.`prd_coupon_code` AS `prd_coupon_code`,`orders`.`prd_total` AS `prd_total`,`orders`.`link_unique` AS `link_unique`,`orders`.`order_status` AS `order_status`,`orders`.`time_order` AS `time_order`,`orders`.`resi` AS `resi`,`orders`.`receipt` AS `receipt`,`mst_province`.`province` AS `province`,`mst_city`.`city` AS `city`,`section_product_items`.`title` AS `title`,`section_product_items`.`description` AS `description`,`section_product_items`.`description2` AS `description2`,`section_product_items`.`price` AS `mst_prd_price`,`section_product_items`.`price_dsc` AS `price_dsc`,`section_product_items`.`image` AS `image`,`section_product_items`.`image_bg` AS `image_bg`,`section_product_items`.`code` AS `code` from (((`orders` join `mst_province` on((`orders`.`cust_province` = `mst_province`.`id`))) join `mst_city` on((`orders`.`cust_city` = `mst_city`.`id`))) join `section_product_items` on((`orders`.`prd_id` = `section_product_items`.`id`))) order by `orders`.`time_order` desc;

-- ----------------------------
-- View structure for v_order
-- ----------------------------
DROP VIEW IF EXISTS `v_order`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_order` AS select `orders`.`id` AS `ord_id`,`orders`.`cust_f_name` AS `cust_f_name`,`orders`.`cust_l_name` AS `cust_l_name`,`orders`.`cust_email` AS `cust_email`,`orders`.`cust_address` AS `cust_address`,`orders`.`cust_province` AS `cust_province`,`orders`.`cust_city` AS `cust_city`,`orders`.`cust_zip` AS `cust_zip`,`orders`.`cust_phone` AS `cust_phone`,`orders`.`cust_shipping` AS `cust_shipping`,`orders`.`payment` AS `payment`,`orders`.`payment_detail` AS `payment_detail`,`orders`.`prd_id` AS `p_prd_id`,`orders`.`prd_title` AS `prd_title`,`orders`.`prd_description` AS `prd_description`,`orders`.`prd_description2` AS `prd_description2`,`orders`.`prd_image` AS `prd_image`,`orders`.`prd_price_tag` AS `prd_price_tag`,`orders`.`prd_price` AS `prd_price`,`orders`.`prd_shipment` AS `prd_shipment`,`orders`.`prd_coupon` AS `prd_coupon`,`orders`.`prd_coupon_code` AS `prd_coupon_code`,`orders`.`prd_total` AS `prd_total`,`orders`.`link_unique` AS `link_unique`,`orders`.`time_order` AS `time_order`,`mst_province`.`province` AS `province`,`mst_city`.`city` AS `city`,`section_product_items`.`id` AS `id`,`section_product_items`.`prd_id` AS `prd_id`,`section_product_items`.`title` AS `title`,`section_product_items`.`description` AS `description`,`section_product_items`.`description2` AS `description2`,`section_product_items`.`price` AS `price`,`section_product_items`.`price_dsc` AS `price_dsc`,`section_product_items`.`image` AS `image`,`section_product_items`.`image_bg` AS `image_bg` from (((`orders` join `mst_province` on((`orders`.`cust_province` = `mst_province`.`id`))) join `mst_city` on((`orders`.`cust_city` = `mst_city`.`id`))) join `section_product_items` on((`orders`.`prd_id` = `section_product_items`.`id`)));

-- ----------------------------
-- View structure for v_section_name
-- ----------------------------
DROP VIEW IF EXISTS `v_section_name`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `v_section_name` AS select `ls`.`id` AS `id`,`ls`.`section_id` AS `section_id`,`ls`.`name` AS `name`,`ls`.`type_sec_id` AS `type_sec_id`,`ls`.`sec_order` AS `sec_order`,`ls`.`date_created` AS `date_created`,`ts`.`name` AS `section_name` from (`list_section` `ls` join `type_section` `ts` on((`ls`.`type_sec_id` = `ts`.`id`))) order by `ls`.`sec_order`;

SET FOREIGN_KEY_CHECKS = 1;
