-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jan 03, 2019 at 08:10 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.0.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dzacommerce`
--

-- --------------------------------------------------------

--
-- Table structure for table `mst_payment`
--

CREATE TABLE `mst_payment` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `status` enum('disable','enable','','') COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_payment`
--

INSERT INTO `mst_payment` (`id`, `name`, `description`, `code`, `status`) VALUES
(1, 'Cash on Delivery', 'Allow yo to pay on the spot when item deliver to you edit', 'cod', 'enable'),
(2, 'Manual Transfer', 'Transfer via ATM or Mobile / Internet banking to account number below', 'transfer', 'enable'),
(3, 'Doku Virtual Account', 'Pay with doku virtual account edit', 'doku_virtual', 'disable'),
(4, 'Doku Credit Card', 'Pay with credit card via doku payment gateway', 'doku_cc', 'disable'),
(5, 'Doku Alfamart', 'Pay with alfamart nearest to you using doku payment gateway', 'doku_alfa', 'disable'),
(6, 'Kredivo', 'Pay with kredivo account', 'kredivo', 'disable');

-- --------------------------------------------------------

--
-- Table structure for table `mst_payment_transfer`
--

CREATE TABLE `mst_payment_transfer` (
  `id` int(11) NOT NULL,
  `bank` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `account_number` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `name_holder` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mst_payment_transfer`
--

INSERT INTO `mst_payment_transfer` (`id`, `bank`, `account_number`, `name_holder`) VALUES
(1, 'Mandiri', '8973429174892', 'Andi Ahmad'),
(2, 'BNI', '567893878651', 'Andi Ahmad'),
(3, 'BRI', '4395804385', 'Andi Ahmad'),
(6, 'Panin', '32084093284', 'Andi Ahmad');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mst_payment`
--
ALTER TABLE `mst_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mst_payment_transfer`
--
ALTER TABLE `mst_payment_transfer`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mst_payment`
--
ALTER TABLE `mst_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `mst_payment_transfer`
--
ALTER TABLE `mst_payment_transfer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
