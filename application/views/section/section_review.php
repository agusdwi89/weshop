<style type="text/css">
	#flexreview{
		background: transparent !important;
	}
	#flexreview .flex-next,#flexreview .flex-prev{
		font-family: 'simple-line-icons';
		speak: none;
		font-style: normal;
		font-weight: normal;
		font-variant: normal;
		text-transform: none;
		line-height: 1;
		/* Better Font Rendering =========== */
		-webkit-font-smoothing: antialiased;
		-moz-osx-font-smoothing: grayscale;
	}
	div.review{padding-left: 50px;}
	#flexreview .flex-prev:before{content: "\e605" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}
	#flexreview .flex-next:before{content: "\e606" !important;font-family: 'simple-line-icons' !important; color:#4cc3d3;}
	
</style>

<?
	$cust = $this->db->get_where('section_review_customers',array('srev_id'=>$data->id));	
	$items = $this->db->get_where('section_review_items',array('srev_id'=>$data->id));	
?>
<section id="review" class="reviews block gray">
	<div class="container">
		<div class="row">
			<h2><?=$data->title;?></h2>
			<div class="block-desc">
				<p><?=$data->description;?></p>
			</div>

			<div id="flexreview" class="flexslider slider">
				<ul class="slides">
					
					<?$i=0;foreach ($items->result() as $r ): $i++;?>

						<?php if ($i % 2 == 1): ?>
							<li style="background: transparent;">		
						<?php endif ?>
							<div id="review-1" class="col-md-6 review">
								<p><?=$r->review;?></p>
								<div class="author">
									<img src="<?=base_url()?>assets/section/<?=$r->image;?>" alt="author">
									<h4><?=$r->name;?></h4>
									<p><?=$r->position;?></p>
								</div>
							</div>
						<?php if ($i % 2 == 0): ?>
							</li>
						<?php endif ?>
					<?endforeach;?>
					<?php if ($i % 2 == 1): ?>
						</li>
					<?php endif ?>
				</ul>
			</div>
			<br>

			<div class="col-md-12 customers">
				<h3><?=$data->title_secondary;?></h3>
				<?foreach ($cust->result() as $k): ?>
					<div class="col-md-3 customer">
						<a href="<?=$k->link;?>" target="_blank">
							<img src="<?=base_url()?>assets/section/<?=$k->image;?>" title="<?=$k->name;?>" alt="customer">
						</a>
					</div>
				<?endforeach;?>
			</div>
		</div>
	</div>
</section>