<!-- begin popads -->
<? $popadsactive = $this->db->get_where('site_config',array('name'=>'pop-ads','subdomain'=>subdomain()))->row();?>
<?php if (strlen($popadsactive->value) > 0 ):?>
	<a style="position: fixed;z-index: 1000000000;top: 50%;left: 50%;margin-left: -35px;z-index: -1000000000;opacity: 0;" class="image-popup-vertical-fit" href="<?=base_url()?>assets/popads/<?=$popadsactive->value;?>">
		<img src="<?=base_url()?>assets/popads/<?=$popadsactive->value;?>" width="75" height="75" />
	</a>
	<script type="text/javascript">
		$(function(){
			init_popads();
			setTimeout(function() {
				$(".image-popup-vertical-fit").trigger('click');
			}, 2000);
		})
	</script>
<?php endif ?>