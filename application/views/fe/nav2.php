<nav id="navigation" style="background-color: #43bccd;" class="navbar2 navbar-default navbar-top">
	<div class="container-fluid">
		<div style="background-color: #43bccd;" class="nav-bg2 top-nav"></div>
		<div class="container">
			<div class="navbar-button">
				<ul class="ul-btn">
					<li><a href="<?=base_url()?>" class="button action-button-nav smooth" title="Buy Now">Back to Homepage</a></li>
				</ul>
			</div>
			<div class="navbar-header">
				<button id="nav-icon" type="button" class="navbar-toggle collapsed nav-icon" data-toggle="collapse" data-target="#menu-navbar" aria-expanded="false">
				</button>
				<a class="navbar-brand smooth" href="#home">
					<?=$this->db->get_where('site_config',array('id'=>1))->row()->value;?>
					<span class="logo-promo-text"><?=$this->db->get_where('site_config',array('id'=>2))->row()->value;?></span>
				</a> <!-- Title / Tagline -->
			</div>
			<div class="collapse navbar-collapse" id="menu-navbar">				
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</nav>