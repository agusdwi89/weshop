<div id="content-payment">
	<style type="text/css">
		#pay-sum, #pay-sum-bank{padding: 15px;padding-bottom: 5px;}
		#pay-sum .card, #pay-sum-bank .card{
			border-radius: 5px;
			border-left: 5px #ffc800 solid;
		}
		#pay-sum .card .card-body,#pay-sum-bank .card .card-body{padding: 10px 15px 15px;}
		.select_payment div{
			padding: 0px !important;
		}
		#pay-sum .card-text,#pay-sum-bank .card-text{margin-bottom: 0px !important}
		#pay-sum-bank select{
			width: 75%;
			margin: 10px auto 15px;
			display: block;
		}
		.paysum{display: none;}
		#pay-coupon-form{display: none;padding: 0px 75px}
	</style>
	<script type="text/javascript">
		$(function(){
			payValue = "";
			$('body').on('click','.select_payment',function (e) {
				e.preventDefault();
				payValue = $(this).data('value');
				$(".select_payment.active").removeClass('active');
				$(this).addClass('active');
				$(".select_payment").removeClass('order-field-error');
				$("#hd-payment").val(payValue);
				$(this).addClass('active');
				$(".paysum").hide();
				var dsc = $(this).data('description');
				if (payValue == 'transfer') {
					$("#pay-sum-bank p.below").text(dsc);
					$("#pay-sum-bank").fadeIn();
					$("#hd-payment_detail").val($("#payment_transfer_option").val());
					confirmPay = "Transfer Bank : "+$("#payment_transfer_option option:selected").text();
				}else if(payValue == 'cod'){
					$("#pay-sum").find('p').text(dsc);
					$("#pay-sum").fadeIn();
					confirmPay = "Cash on Delivery";
				}else if(payValue == 'kredivo'){
					$("#pay-sum").find('p').text(dsc);
					$("#pay-sum").fadeIn();
					confirmPay = "Kredivo";
				}else{
					$("#pay-sum").find('p').text(dsc);
					$("#pay-sum").fadeIn();
					confirmPay = "Pay with Doku Wallet";
				}
			});

			$('body').on('click','#pay-coupon-have',function (e) {
				e.preventDefault();
				$(this).fadeOut(function(){
					$("#pay-coupon-form").fadeIn(function(){
						$("#coupon-input").focus();
					});
				});
			});
			$('body').on('click','#order-button-payment',function (e) {
				e.preventDefault();
				if (payValue == "") {
					$(".select_payment").addClass('order-field-error');
				}else{
					$("#content-payment").fadeOut(function(){
						setConfirmation();
						$("#content-confirmation").fadeIn();
					});
				}
			});
			$('body').on('click','#coupon-submit',function (e) {
				e.preventDefault();
				var csrf 				= $("#global-form").find("input");
				var usercode 			= $(this).parent().find('input');
				var opt 				= {};
				opt[csrf.attr('name')] 	= csrf.val();
				opt['code']				= usercode.val();
				$('body').css('cursor', 'wait');
				$.post("<?=base_url()?>home/check_coupon",  opt,function(data){
					$('body').css('cursor', 'default');
					if (data.status == 'valid') {
						$("#hd-prd_coupon_code").val(usercode.val());
						$('#pay-coupon-form').fadeOut(function(){
							$("#pay-coupon-valid").fadeIn().html("Selamat anda mendapatkan potongan <span class='sale-price' >Rp. "+ formatNumber(data.value)+'</span>');
							coupon_final = data.value;
						})
					}else{
						$("#hd-prd_coupon_code").val("");
						$("#coupon-input").focus();
						coupon_final = 0;
						usercode.addClass('order-field-error');
					}
				},"json");
			});
		})
	</script>
	<div id="payment-step">
		<div class="col-md-12"><h3>Metode Pembayaran</h3></div>
		<div class="col-md-6">
			<b></b>
		</div>
		<div class="clearfix"></div>
		<div>
			<? $mstpayment = $this->db->get_where('mst_payment',array('status'=>'enable','subdomain'=>subdomain())); ?>
			<?php foreach ($mstpayment->result() as $m): ?>
				<div class="col-md-6">

					<?php if (strpos($m->code, "doku") !== false): ?>
						<div class="select_payment doku" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
							<div class="col-md-3"><img src="https://doku.promo/wp-content/uploads/2018/02/LOGO-DOKU-2017-e1518166626653.png" style="width:!00px"></div>	
							<div style="font-size:11px !important" class="col-md-9 "><?=$m->name;?></div>
							<div class="clearfix"></div>
						</div>		
					<?php elseif (strpos($m->code, "kredivo") !== false): ?>

						<div class="select_payment doku" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
							<div class="col-md-3"><img src="https://yt3.ggpht.com/a-/AN66SAytWq1KxaSUEAB120OqP3WmjYXfN5vu8RifCQ=s900-mo-c-c0xffffffff-rj-k-no" style="width:!00px"></div>	
							<div class="col-md-9 "><?=$m->name;?></div>
							<div class="clearfix"></div>
						</div>

					<?php else: ?>
						<div class="select_payment" data-value="<?=$m->code;?>" data-description="<?=$m->description;?>">
							<?=$m->name;?>
						</div>
					<?php endif ?>
				</div>	
			<?php endforeach ?>
		</div>
		<div class="clearfix"></div>
		<div id="pay-sum" class="paysum">
			<div class="card text-white bg-warning">
				<div class="card-body">
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
				</div>
			</div>
		</div>
		<div id="pay-sum-bank" class="paysum">
			<div class="card text-white bg-warning">
				<div class="card-body">
					<p class="card-text below">Please select bank below</p>
					<? $rek = $this->db->get_where('mst_payment_transfer',array('subdomain'=>subdomain()));?>
					<select id="payment_transfer_option" class="order-country" name="order-country">
						<?foreach ($rek->result() as $d): ?>
							<option value="<?=$d->bank;?> : <?=$d->account_number;?> -- <?=$d->name_holder;?>"><?=$d->bank;?> : <?=$d->account_number;?> -- <?=$d->name_holder;?></option>
						<?endforeach;?>
					</select>
				</div>
			</div>
		</div><br>
		<div id="pay-coupon">
			<center><b><a id="pay-coupon-have" href="#">Punya Kode Voucher?</a></b></center>
			<div id="pay-coupon-form" class="newsletter-field-group">
				<input id="coupon-input" type="text" name="newsletter-email" placeholder="masukkan kode kupon" style="width:65%;border-radius:20px;font-size:13px;margin-left: 35px;">
				<button id="coupon-submit" type="submit" class="button submit-button" style="margin-left: -20px;height: 37px;line-height: 14px;">check</button>
			</div>
			<center><b><a id="pay-coupon-valid" href="#"></a></b></center>
		</div>
		<div class="col-md-12 order-button-block">
			<button id="order-button-payment" type="submit" class="button order-button" title="Order">Lanjutkan</button>
		</div>
	</div>
</div>