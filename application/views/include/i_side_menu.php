<? 
    $current = (isset($this->current_menu)? $this->current_menu : ""); 
?>

<div class="left side-menu">
    <div class="slimscroll-menu" id="remove-scroll">
        <div id="sidebar-menu">
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Navigation</li>
                <li>
                    <a class="<?=($current == "dashboard")? 'active':'';?>" href="<?=base_url()?>manage/dashboard"><i class="fi-air-play"></i><span> Dashboard </span> </a>
                </li>
                <li>
                    <a class="<?=($current == "section")? 'active':'';?>" href="<?=base_url()?>manage/section"><i class="fi-layout"></i> <span> Manage Section </span></a>                                
                </li>
                <li>
                    <a class="<?=($current == "order")? 'active':'';?>" href="<?=base_url()?>manage/order"><i class="fi-bag"></i> <span> Data Order </span></a>
                </li>
                <li>
                    <a class="<?=($current == "customer")? 'active':'';?>" href="<?=base_url()?>manage/customer"><i class="fi-head"></i> <span> Data Customer </span></a>
                </li>
<!--                 <li>
                    <a class="<?=($current == "location")? 'active':'';?>" href="<?=base_url()?>manage/master/province"><i class="fi-map"></i> <span> Master Location </span></a>
                </li> -->
                <li>
                    <a class="<?=($current == "shipping")? 'active':'';?>" href="<?=base_url()?>manage/master/shipping"><i class="fi-briefcase"></i> <span> Master Shipping </span></a>
                </li>
                <li>
                    <a class="<?=($current == "payment")? 'active':'';?>" href="<?=base_url()?>manage/master/payment"><i class="fi-tag"></i> <span> Manage Payment </span></a>
                </li>
                <li>
                    <a class="<?=($current == "menu")? 'active':'';?>" href="<?=base_url()?>manage/topmenu"><i class="fi-grid"></i> <span> Top Menu  </span></a>
                </li>
                <li>
                    <a class="<?=($current == "subdomain")? 'active':'';?>" href="<?=base_url()?>manage/subdomain"><i class="fi-map"></i> <span> Subdomain </span></a>
                </li> 
                <li>
                    <a class="<?=($current == "popads")? 'active':'';?>" href="<?=base_url()?>manage/popads"><i class="fi-cog"></i> <span> Pop Up Ads </span></a>
                </li>
                <li>
                    <a class="<?=($current == "config")? 'active':'';?>" href="<?=base_url()?>manage/site_config"><i class="fi-cog"></i> <span> Site Configuration </span></a>
                </li>
                <li>
                    <a class="<?=($current == "logout")? 'active':'';?>" href="<?=base_url()?>manage/login/off"><i class="fi-power"></i> <span> Log Out </span></a>
                </li>
            </ul>
        </div>
        <div class="clearfix"></div>
    </div>
</div>