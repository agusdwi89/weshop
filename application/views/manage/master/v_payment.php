<style type="text/css">
#list-payment li img{
	margin-right: 10px;
}
#list-payment li i.fa{
	font-size: 38px;
	text-align: center;
	line-height: 50px;
	margin-right: 10px;
}
.list-group-item{
	cursor: pointer;
}
.list-group-item.active{
	background: #348cd4;
	border-color: #348cd4;
}
.list-group span.switchery{
	position: absolute;
	right: 25px;
}
#wizard-vertical,#wizard-vertical2{
	padding: 20px;
	border: 1px solid #dadada;
	border-radius: 5px;
	display: none;
}
.delete-bank{
	color: gray;
	margin-top: 2px;
	font-size: 17px;
}

</style>
<script type="text/javascript">
	$(function(){
		$('body').on('click','.list-group-item',function (e) {
			$('.list-group-item.active').removeClass('active');
			$(this).addClass('active');
			var frm = $("#wizard-vertical")
			frm.show();
			frm.find('h4').text($(this).data('name'));
			$("#frm_id").val($(this).data('id'));
			$("#frm_desc").val($(this).data('description')).focus();

			if ($(this).data('code') == 'transfer') {
				$("#wizard-vertical2").show();
			}else{
				$("#wizard-vertical2").hide();
			}
		});
		$('body').on('click','.delete-bank',function (e) {
			return confirm('Are you sure to delete this item ?');
		});

		$('[data-plugin="switchery"]').each(function (idx, obj) {
			new Switchery($(this)[0], $(this).data());
		});

		$('.checkcolor').change(function() {
			var dataf = {};
			var gbl = $("#global-form").find('input');
			document.body.style.cursor='wait';
			var fstatus = "disable";
			var fid 	= $(this).data('id');
			if($(this).is(":checked")) 
				fstatus = "enable";

			dataf['status'] 		= fstatus;
			dataf['id'] 			= fid;
			dataf[gbl.attr('name')] = gbl.val();

			$.post( "<?=base_url()?>manage/master/payment_status", dataf).done(function( data ) {
				document.body.style.cursor='default';
			});

		});
	})
</script>
	
<?php if ($id > 0): ?>
	<script type="text/javascript">
		$(function(){
			$("#list-<?=$id?>").click();
		})
	</script>
<?php endif ?>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Payment</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">

						<?php if ($this->sub_domain == "all"): ?>

							<div class="col-md-12">
								<center>
									<br>
									<h3>please select sub domain above</h3>
									<br>
								</center>
							</div>

						<?php else: ?>

						<h4 class="m-t-0 header-title">
							<b>List of Available Payment</b>
						</h4>
						<p><i>*click each payment to detail option</i></p>
						<div class="row">
							<div class="col-sm-5">
								<ul id="list-payment" class="list-group">
									<?php foreach ($payment->result() as $d): ?>
										<li id="list-<?=$d->id;?>" class="list-group-item d-flex align-items-center" data-code="<?=$d->code;?>" data-name="<?=$d->name;?>" data-id="<?=$d->id;?>" data-description="<?=$d->description;?>">
											<?php if (strpos($d->code, 'doku') !== false): ?>
												<img width="50" height="50" src="https://doku.promo/wp-content/uploads/2018/02/LOGO-DOKU-2017-e1518166626653.png">	
											<?php elseif (strpos($d->code, 'kredivo') !== false): ?>
												<img width="50" height="50" src="https://yt3.ggpht.com/a-/AN66SAytWq1KxaSUEAB120OqP3WmjYXfN5vu8RifCQ=s900-mo-c-c0xffffffff-rj-k-no">	
											<?php else: ?>
												<i class="fa fa-cc-mastercard"></i>
											<?php endif ?>
											<span><?=$d->name;?></span>
											<input data-id="<?=$d->id;?>" class="checkcolor" type="checkbox" data-plugin="switchery" data-color="#ff5d48"  <?=($d->status == "enable") ? 'checked' : '';;?>/>
										</li>
									<?php endforeach ?>
								</ul>

							</div>
							<div class="col-sm-7">
								<div class="">
									<section id="wizard-vertical" role="tabpanel" aria-labelledby="wizard-vertical-h-0" class="body current" aria-hidden="false">
										<?=form_open('',array('id'=>'desc_form'))?>
										<input type="hidden" name="id" id="frm_id">
										<h4>Doku</h4>
										<div class="form-group row">
											<label class="col-lg-12 control-label " for="confirm1">Description</label>
											<div class="col-lg-12">
												<textarea name="description" id="frm_desc" class="form-control" rows="3"></textarea>
												<button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
											</div>
										</div>
										<?=form_close()?>
									</section><br>
									<section id="wizard-vertical2" role="tabpanel" aria-labelledby="wizard-vertical-h-0" class="body current" aria-hidden="false">
										<?=form_open(base_url('manage/master/payment_transfer'),array('id'=>'desc_form'))?>
										<h4>List of Bank Transfer</h4>
										<div class="form-group row">
											<label class="col-lg-12 control-label " for="confirm1">Add Bank</label>
											<div class="col-lg-12">
												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-3 control-label">Bank</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="frm_trf_bank" placeholder="bank" name="bank">
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-3 control-label">Account Number</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="frm_trf_account_number" placeholder="account number" name="account_number">
													</div>
												</div>

												<div class="form-group row">
													<label for="inputEmail3" class="col-sm-3 control-label">Name</label>
													<div class="col-sm-9">
														<input type="text" class="form-control" id="frm_trf_name_holder" placeholder="name holder" name="name_holder">
													</div>
												</div>
												<button type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
											</div>
											<div class="col-lg-12">
												<br>
												<table class="table table table-hover m-0">
													<tr>
														<th>No</th>
														<th>Bank</th>
														<th>Account Number</th>
														<th>Account Name</th>
														<th>#</th>
													</tr>
													<?php $i=0; foreach ($transfer->result() as $t): $i++;?>
													<tr>
														<th scope="row"><?=$i;?></th>
														<td><?=$t->bank;?></td>
														<td><?=$t->account_number;?></td>
														<td><?=$t->name_holder;?></td>
														<td>
															<a title="delete item" class="fa fa-times-circle delete-bank dz-tip" href="<?=base_url()?>manage/master/payment_transfer_delete/<?=$t->id;?>"></a>
														</td>
													</tr>
													<?php endforeach ?>
												</table>
											</div>
										</div>
										<?=form_close()?>
									</section>
								</div>							
							</div>
						</div>

						<?endif?>
					
					</div>
				</div>
			</div>
		</div>
	</div>
</div>