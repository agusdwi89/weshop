<div class="row">
	<div class="col-sm-12">				
		<div class="row">
			<div class="col-xl-3 col-md-6">
				<div class="card widget-box-two widget-two-purple">
					<div class="card-body">
						<i class="mdi mdi-chart-line widget-two-icon"></i>
						<div class="wigdet-two-content">
							<p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow" title="Statistics">Total Potencial Sales</p>
							<h2 class="text-white"><span data-plugin="counterup">Rp. <?=format_number($topstat['sales_potency']);?></span></h2>
							<p class="text-white m-0">Orders with uncompleted status</p>
						</div>
					</div>
				</div>
			</div><!-- end col -->

			<div class="col-xl-3 col-md-6">
				<div class="card widget-box-two widget-two-info">
					<div class="card-body">
						<i class="mdi mdi-cart widget-two-icon"></i>
						<div class="wigdet-two-content">
							<p class="m-0 text-white text-uppercase font-600 font-secondary text-overflow" title="User Today">Fix Sales</p>
							<h2 class="text-white"><span data-plugin="counterup">Rp. <?=format_number($topstat['sales_real']);?></span> </h2>
							<p class="text-white m-0">Orders with completed status</p>
						</div>
					</div>
				</div>
			</div><!-- end col -->

			<div class="col-xl-3 col-md-6">
				<div class="card widget-box-two widget-two-pink">
					<div class="card-body">
						<i class="mdi mdi-account-circle widget-two-icon"></i>
						<div class="wigdet-two-content">
							<p class="m-0 text-uppercase text-white font-600 font-secondary text-overflow" title="Request Per Minute">Total Ordered User</p>
							<h2 class="text-white"><span data-plugin="counterup"><?=$topstat['user_total'];?></span></h2>
							<p class="text-white m-0">Distinc by email</p>
						</div>
					</div>
				</div>
			</div><!-- end col -->

			<div class="col-xl-3 col-md-6">
				<div class="card widget-box-two widget-two-success">
					<div class="card-body">
						<i class="mdi mdi mdi-chart-bar widget-two-icon"></i>
						<div class="wigdet-two-content">
							<p class="m-0 text-white text-uppercase font-600 font-secondary text-overflow" title="New Downloads">Total Visitor Today</p>
							<h2 class="text-white"><span data-plugin="counterup"><?=$topstat['visit_today'];?></span></h2>
							<p class="text-white m-0"><b>Total unique visitor today</p>
						</div>
					</div>
				</div>
			</div><!-- end col -->

		</div>

	</div>
</div>
<div class="row">
	
	<div class="col-xl-6">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title m-t-0 m-b-20">10 Last Orders</h4>

				<div class="table-responsive">
					<table class="table m-b-0">
						<thead>
							<tr>
								<th style="text-align:center">Order ID</th>
								<th style="text-align:center">Customer</th>
								<th style="text-align:center">Total</th>
								<th style="text-align:center">Status</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($top10['order']->result() as $v): ?>
								<tr>
									<td><?="#SB1".sprintf('%05d', $v->id);?></td>
									<td><?=$v->cust_f_name;?></td>
									<td>Rp. <?=format_number($v->prd_total);?></td>
									<td style="text-align:center">
										<?if ($v->order_status == "wait-payment"): ?>
											<span class="badge badge-danger">menunggu-pembayaran</span>
										<?elseif($v->order_status == "payment-success"):?>
											<span class="badge badge-info">pembayaran-sukses</span>
										<?elseif($v->order_status == "delivery-process"):?>
											<span class="badge badge-info">proses-pengiriman</span>
										<?else: ?>
											<span class="badge badge-success">pesanan-diterima</span>
										<?endif;?>
									</td>
								</tr>	
							<?php endforeach ?>
							<tr>
								<td colspan="6">
									<a style="display: block;width: 100%;margin: 10px auto 0;" href="<?=base_url('manage/order')?>" class="btn btn-purple waves-effect w-md waves-light">View All Orders</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

	<div class="col-xl-6">
		<div class="card">
			<div class="card-body">
				<h4 class="header-title m-t-0 m-b-20">10 Last Customers</h4>

				<div class="table-responsive">
					<table class="table m-b-0">
						<thead>
							<tr>
								<th>Nama</th>
								<th>Email</th>
								<th>Phone</th>
							</tr>
						</thead>
						<tbody>
							<?php foreach ($top10['user']->result() as $v): ?>
								<tr>
									<td><?=$v->cust_f_name;?></td>
									<td>
										<a href="mailto:(<?=$v->cust_email;?>)"><?=$v->cust_email;?></a>
									</td>
									<td><?=$v->cust_phone;?></td>
								</tr>	
							<?php endforeach?>
							<tr>
								<td colspan="6">
									<a style="display: block;width: 100%;margin: 10px auto 0;" href="<?=base_url('manage/customer')?>" class="btn btn-purple waves-effect w-md waves-light">View All Customers</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

</div>