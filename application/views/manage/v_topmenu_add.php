<style type="text/css">
	#section_internal, #section_eksternal{display: none;}
</style>
<script type="text/javascript">
	$(function(){
		$("#section_internal").show();

		$( "#opt_link" ).change(function() {
			if ($(this).val() == "Internal") {
				$("#section_eksternal").hide();
				$("#section_internal").show();
			}else{
				$("#section_internal").hide();
				$("#section_eksternal").show();
			}
		});
	})
</script>	
<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Top Menu</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<h4 class="m-t-0 header-title">
							<b>New Item</b>
							<a href="<?=base_url('manage/topmenu')?>" style="float: right" class="btn btn-info waves-effect waves-light btn-sm">  <span>Back</span> </a>
						</h4>
						<br>
						<?=form_open('',array('class'=>'form-horizontal'))?>					
						<div class="form-group row">
							<label class="col-md-2 control-label">Name</label>
							<div class="col-md-5">
								<input type="text" placeholder="menu name" class="form-control" name="def[name]" value="">
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 control-label">Link</label>
							<div class="col-md-5">
								<select id="opt_link" name="def[link]" class=" form-control" title="">
									<option>Internal</option>
									<option>Eksternal</option>
								</select>
							</div>
						</div>
						<div class="form-group row">
							<label class="col-md-2 control-label">URL</label>
							<div class="col-md-5">
								<div id="section_internal">
									<select name="Internal" class=" form-control" title="">
										<?php foreach ($section->result() as $s): ?>
											<option><?=$s->section_name;?></option>
										<?php endforeach ?>
									</select>	
								</div>
								<div id="section_eksternal">
									<input type="text" placeholder="eksternal url" class="form-control" name="Eksternal" value="">
								</div>
							</div>
						</div>
						<!-- <div class="form-group row">
							<label class="col-md-2 control-label">Button Style</label>
							<div class="col-md-5">
								<select name="def[button]" class=" form-control" title="">
									<option>Yes</option>
									<option selected="selected">No</option>
								</select>
							</div>
						</div> -->
						<button id="btn-submit-tech" type="submit" class="btn btn-purple waves-effect waves-light m-t-20">Submit</button>
						<?=form_close()?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>