<style type="text/css">
	#datatable_wrapper{opacity: 0}
	#datatable{opacity: 0}
	p.txwrap{
		max-width:200px !important;
		overflow-wrap: break-word !important;
		word-wrap: break-word !important;
		white-space: pre-wrap;
		line-height: 15px;
		font-size: 11px;
		font-style: italic;
	}
	a.btn-confirm-download span{
		font-size: 10px;
		display: block;
		float: right;
		margin-left: 5px;
	}
	a.btn-confirm-download i{
		margin-top: 9px;
	}
</style>
<script type="text/javascript">
	$(function(){
		$('#datatable').dataTable({
			"initComplete": function(settings, json) {
				$("#loader").remove();
				$("#datatable_wrapper").css('opacity', '1');
				$("#datatable").css('opacity', '1');
			}
		});

		var start = moment();
		var end = moment();

		function cb(start, end) {
			$('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		}

		$('#reportrange').daterangepicker({
			startDate: start,
			endDate: end,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			}
		}, cb);

		cb(start, end);

		$('#reportrange').on('apply.daterangepicker', function(ev, picker) {
			alert(picker.startDate.format('YYYY-MM-DD'));
			alert(picker.endDate.format('YYYY-MM-DD'));
		});
	})
</script>

<div class="content">
	<div class="container-fluid">
		<div class="row">
			<div class="col-12">
				<div class="page-title-box">
					<h4 class="page-title">Manage Customer</h4>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-body table-responsive">
						<div class="row">
							<div class="col-md-12">
								<h4 style="line-height:30px" class="m-t-0 header-title pull-left">List All Customer [ subdomain : <?=(($this->sub_domain == "all")? "All sub-domain" : $this->sub_domain.".".$_SERVER['HTTP_HOST']);?> ]</h4>
								<div id="reportrange" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc;" class="pull-right">
									<i class="fa fa-calendar"></i>&nbsp;
									<span></span> <i class="fa fa-caret-down"></i>
								</div>	
								<a title="download csv" href="<?=base_url('manage/customer/download')?>" style="float: right;margin-right:5px" class="btn btn-info waves-effect waves-light btn-sm dz-tip"><i class="fa fa-cloud-download"></i></a>
							</div>
						</div>
						<br>
						<div id='loader'><center>Loading data . . . <br></center></div>

						<table id="datatable" class="table table-striped table-bordered dt-responsive nowrap" style="border-collapse: collapse; border-spacing: 0; width: 100%;">
							<thead>
								<tr>
									<th>No</th>
									<th>Sub-Domain</th>
									<th>Customer Name</th>
									<th>Email</th>
									<th>Phone</th>
									<th>Province</th>
									<th>City</th>
									<th class="text-center">Detail Order</th>
								</tr>
							</thead>
							<tbody>
								<?$i=0;foreach ($db as $d): $i++;?>
									<tr>
										<td><?=$i;?></td>
										<td><?=$map_product_domain[$d->prd_id]['sd']->subdomain?></td>
										<td>
											<b><?=$d->cust_f_name;?></b><br>
										</td>
										<td>
											<a href="mailto:(<?=$d->cust_email;?>)"><?=$d->cust_email;?></a>
										</td>
										<td><?=$d->cust_phone;?></td>
										<td><?=$d->province;?></td>
										<td><?=$d->city;?></td>
										<td>
											<center>
											<a href="<?=base_url()?>manage/customer/detail/<?=$d->id;?>" class="btn btn-xs btn-icon waves-effect waves-light btn-purple dz-tip" title="Customer order detail"> <i class="fa fa-search"></i>
												customer order
											</a>
											</center>
										</td>
									</tr>
								<?endforeach;?>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>