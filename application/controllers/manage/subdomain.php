<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Subdomain extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "subdomain";
		$this->sub_domain = $this->session->userdata('session_subdomain');
	}

	public function index()
	{        
		$data['db'] 		= $this->db->get_where('_subdomain',array('active'=>1));
		$data['local_view'] = 'v_subdomain';
		$this->load->view('v_manage',$data);
	}

	function add(){
		if (is_post()) {
			$sd = $this->input->post('subdomain');

			$cek = $this->db->get_where('_subdomain',array('subdomain'=>$sd))->num_rows();
			if ($cek > 0) {
				$this->session->set_flashdata('message','Subdomain exist');
				redirect(base_url('manage/subdomain'));
			}
			
			$this->start_init_record($sd);

			$input['subdomain'] = $sd;
			$this->db->insert('_subdomain',$input);
			$this->session->set_flashdata('message','Data saved successfully');
			redirect(base_url('manage/topmenu'));
		}
	}

	function delete($id){
		if ($id == 1) {
			$this->session->set_flashdata('message',"you can't delete default subdomain 'www'");
			redirect(base_url('manage/subdomain'));
		} 
		
		$data = array('active' => 0);
		$this->db->where('id', $id);
		$this->db->update('_subdomain', $data);
		$this->session->set_flashdata('message','Data delete successfully');
		redirect(base_url('manage/subdomain'));
	}

	function start_init_record($sd){
		// site config default record
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'site-title', 'site title | {$sd}', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'top-title', '{$sd}', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'top-subtitle', 'sub-title {$sd}', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'meta-description', 'description {$sd}', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'meta-keywords', 'keywoard {$sd}', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'fav-icon', NULL, NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'buy-now', 'product', 'on', 'buy now')");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'email-footer', 'Jika butuh bantuan silahkan hubungi (021) 29496354', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'user-buy', '950', NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'user-see-bottom', '1100', '100', NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'user-see-top', '1200', '100', NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'fb-track', NULL, NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'google-track', NULL, NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'pod-ads', NULL, NULL, NULL)");
		$this->db->query("INSERT INTO `site_config` VALUES (NULL,'{$sd}', 'reserved-2', NULL, NULL, NULL)");

		// payment default record
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Cash on Delivery', 'Pembayaran dilakukan ketika barang sampai dirumah.', 'cod', 'enable')");
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Manual Transfer', 'Lakukan Transfer via ATM or Mobile / Internet banking ke nomor rekening yang anda pilih.', 'transfer', 'enable')");
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Doku Virtual Account', 'Pay with doku virtual account edit', 'doku_virtual', 'enable')");
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Doku Credit Card', 'Pay with credit card via doku payment gateway', 'doku_cc', 'enable')");
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Doku Alfamart', 'Pay with alfamart nearest to you using doku payment gateway', 'doku_alfa', 'enable')");
		$this->db->query("INSERT INTO `mst_payment` VALUES (NULL,'{$sd}', 'Kredivo', 'Pay with kredivo account', 'kredivo', 'enable')");
	}
}