<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Section_video extends CI_Controller {

	function __construct(){
		parent::__construct();
		$this->current_menu = "section";
	}
	function edit($id){
		if (is_post()) {
			$item = $this->input->post();

			$this->db->where('id', $id);
			$this->db->update('section_video', $item); 
			$this->session->set_flashdata('message','Data Saved Successfully');
			redirect(base_url("manage/section_video/edit/$id"));
		}
		$data['items'] 		= $this->db->get_where('section_video',array('id'=>$id))->row();
		$data['local_view'] = 'v_section_video';
		$this->load->view('v_manage',$data);
	}
}