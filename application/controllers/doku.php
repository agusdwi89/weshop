<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Doku extends CI_Controller {

	function __construct(){
		parent::__construct();

		$this->mall_id 		= "6733";
		$this->shared_key 	= "10YlikOnU53P";
		$this->doku_url		= "https://staging.doku.com/Suite/Receive";
		
	}

	function pay($link_unique){
		$db 	= $this->db->get_where('orders',array("link_unique"=>$link_unique));
		if ($db->num_rows == 1) {
			$dbr = $db->row();
			if ($this->cek_payment_doku($dbr->payment)) {

				$this->payment_channel = $this->cek_payment_channel($dbr->payment);

				$data['dbr'] 	= $dbr;
				$data['child'] 	= "v_pay_redirect";
				$this->load->view('doku/v_wrapper',$data);
			
			}else show_404();
		}else show_404();
	}

	function request_pay(){
		if (is_post()) {
			$data['trxstatus']			= "Requested";
			$data['transidmerchant']	= $this->input->post('transidmerchant');
			$this->db->insert('doku',$data);

			$id = $this->input->post('id');
			$dt = array('payment_detail' => $data['transidmerchant']);
			$this->db->where('id', $id);
			$this->db->update('orders', $dt); 
			echo json_encode(array("message"=>"success"));
		}		
	}

	function notify(){
		if (is_post()) {
			if ($this->cekServer()){
				if($_POST['TRANSIDMERCHANT']) {
					$order_number = $_POST['TRANSIDMERCHANT'];
				}else{
					$order_number = 0;
				}

				$data['totalamount'] 		= $_POST['AMOUNT'];
				$data['words']    			= $_POST['WORDS'];
				$data['statustype'] 		= $_POST['STATUSTYPE'];
				$data['response_code'] 		= $_POST['RESPONSECODE'];
				$data['approvalcode']   	= $_POST['APPROVALCODE'];
				$data['trxstatus']         	= $_POST['RESULTMSG'];
				$data['payment_channel'] 	= $_POST['PAYMENTCHANNEL'];
				$data['paymentcode'] 		= $_POST['PAYMENTCODE'];
				$data['session_id'] 		= $_POST['SESSIONID'];
				$data['bank_issuer'] 		= $_POST['BANK'];
				$data['cardnumber'] 		= $_POST['MCN'];
				$data['payment_date_time']	= $_POST['PAYMENTDATETIME'];
				$data['verifyid'] 			= $_POST['VERIFYID'];
				$data['verifyscore'] 		= $_POST['VERIFYSCORE'];
				$data['verifystatus'] 		= $_POST['VERIFYSTATUS'];

				// Validasi Wors
	   			$MALLID 		= $this->mall_id;
	   			$SHAREDKEY 		= $this->shared_key;

	   			$WORDS_GENERATED = sha1($data['totalamount'].$MALLID.$SHAREDKEY.$order_number.$data['trxstatus'].$data['verifystatus']);
	   			// debug_array($WORDS_GENERATED);

	   			if ( $data['words'] == $WORDS_GENERATED ) {
	   				$cekTransID = $this->db->get_where('doku',array('transidmerchant'=> $order_number,'trxstatus'=>'Requested'));
	   				if ($cekTransID->num_rows() != 1) {
	   					$this->logged('notify_order_notfound',$_POST);
	   					echo 'order not found';
	   				} else {
	   					if ($data['trxstatus'] == "SUCCESS") {
	   						$this->db->where('transidmerchant', $order_number);
	   						$this->db->update('doku', $data);
	   						$this->update_data_order($order_number);

	   						$this->logged('notify_payment_failed',$_POST);
	   						echo "pembayaran sukses";
	   					} else {
	   						$data = array('trxstatus' => 'Failed');
	   						$this->db->where('transidmerchant', $order_number);
	   						$this->db->update('doku', $data);
	   						
	   						$this->logged('notify_payment_success',$_POST);
	   						echo "pembayaran gagal";
	   					}
	   				}
	   			} else {
	   				$this->logged('notify_words_unmatch',$_POST);
	   				echo "Stop | Words not match";
	   			}
	   		}else{
	   			$this->logged('notify_server_not_pass',$_POST);
	   			echo "server not pass";
	   		}
	   	}else show_404();
   	}

   	function redirect(){

   		if (is_post()) {
   			$this->logged('redirect',$_POST);  		
   			
   			$data['dbr'] 	= $this->db->get_where('orders',array('payment_detail'=>$_POST['TRANSIDMERCHANT']));;
   			$data['child'] 	= "v_pay_success";
   			$this->load->view('doku/v_wrapper',$data);

   		}else show_404();
   	}

   	function cek_status(){
   		return "";
   	}

   	private function logged($state,$param){
   		$data['state']	= $state;
   		$data['param']	= json_encode($param);

   		$this->db->insert('doku_log',$data);
   	}

   	function tes(){
   		// debug_array($this->cek_payment_channel('doku_alfa'));

   		$s = '{"PAYMENTDATETIME":"20190212215817","PURCHASECURRENCY":"360","LIABILITY":"CUSTOMER","PAYMENTCHANNEL":"15","AMOUNT":"317000.00","PAYMENTCODE":"","MCN":"542640******8754","WORDS":"dd6e13f50b3c31fc341550fdedc1e4c6dbb80b59","RESULTMSG":"SUCCESS","VERIFYID":"","TRANSIDMERCHANT":"Bl533WRyqKoM","BANK":"Bank BNI","STATUSTYPE":"P","APPROVALCODE":"938053","EDUSTATUS":"NA","THREEDSECURESTATUS":"TRUE","VERIFYSCORE":"-1","CURRENCY":"360","RESPONSECODE":"0000","CHNAME":"agus dwi prayogo","BRAND":"MASTERCARD","VERIFYSTATUS":"NA","SESSIONID":"FBbVuAqC9SHhi6pUGsQd"}';
   		debug_array(json_decode($s));
   	}

   	private function cekServer(){
   		return true;
   		$ip_range = "103.10.129.16";
   		if ($_SERVER['REMOTE_ADDR'] != '103.10.129.16' && (substr($_SERVER['REMOTE_ADDR'],0,strlen($ip_range)) !== $ip_range))
   			return false;
   		else return true;
   	}

   	private function update_data_order($order_number){
   		$ds = array('order_status'=>'payment-success');
   		$this->db->where('payment_detail', $order_number);
   		$this->db->update('orders', $ds);
		// notify email next
		$this->email($order_number);
   	}

   	private function cek_payment_doku($d){
   		return (strpos($d,"doku") === false)? false : true ;
   	}

   	private function cek_payment_channel($d){
   		switch ($d) {
   			case 'doku_cc':
   				return "15";
   				break;
   			case 'doku_virtual':
   				return "36";
   				break;
   			case 'doku_alfa':
   				return "35";
   				break;
   		}
   	}

   	function email($order_number){
		$data['db'] 			= $this->db->get_where('v_manage_order',array('payment_detail'=>$order_number))->row();
		$data['email_state'] 	= "payment-success";
		
		$d = $this->load->view('v_email_pay_doku_success',$data,true);

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'ssl://smtp.googlemail.com',
			'smtp_port' => 465,
			'smtp_user' => 'shopbay.id',
			'smtp_pass' => 'Zoomy123456!',
			'mailtype'  => 'html', 
			'charset'   => 'iso-8859-1'
			);
		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");

		$this->email->from('shopbay.id@gmail.com', 'Shop Bay Indonesia');
		$this->email->to($data['db']->cust_email); 

		$this->email->subject('Tagihan dan Petunjuk Pembayaran : '.$data['db']->prd_title);
		$this->email->message($d);  

		$this->email->send();
	}
}